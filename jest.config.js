module.exports = {
  clearMocks: true,

  moduleFileExtensions: ['js', 'json', 'jsx', 'ts', 'tsx'],

  roots: ['src'],

  collectCoverageFrom: ['./src/**/*.{ts,tsx}'],

  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
};
