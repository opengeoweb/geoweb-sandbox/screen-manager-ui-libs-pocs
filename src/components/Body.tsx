/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import { Provider, connect } from 'react-redux';
import {
  MapViewConnect,
  mapActions,
  mapTypes,
  mapSelectors,
} from '@opengeoweb/core';
import { store } from '@opengeoweb/core/store';

import BboxComponent from './BboxComponent';

const baseLayerGrey = {
  id: 'base-layer-2',
  name: 'WorldMap_Light_Grey_Canvas',
  type: 'twms',
  layerType: mapTypes.LayerType.baseLayer,
};

const overLayer = {
  service: 'https://geoservices.knmi.nl/cgi-bin/worldmaps.cgi?',
  name: 'ne_10m_admin_0_countries_simplified',
  format: 'image/png',
  enabled: true,
  id: 'base-layer-1',
  layerType: mapTypes.LayerType.overLayer,
};

const radarLayer = {
  service: 'https://geoservices.knmi.nl/cgi-bin/RADNL_OPER_R___25PCPRR_L3.cgi?',
  name: 'RADNL_OPER_R___25PCPRR_L3_COLOR',
  format: 'image/png',
  enabled: true,
  style: 'knmiradar/nearest',
  id: 'radar-layer-1',
  layerType: mapTypes.LayerType.mapLayer,
};

const northernEuropeBbox = {
  srs: 'EPSG:3857',
  bbox: {
    left: -6870440.749573884,
    bottom: 5623435.0799058415,
    right: 7997443.616050693,
    top: 11525334.644193,
  },
};

const toggleBbox = {
  srs: 'EPSG:3857',
  bbox: {
    left: -420557.07101151417,
    bottom: 6505844.241352999,
    right: 1544275.953754299,
    top: 7485031.24514819,
  },
};

interface RadarProps {
  setLayers: typeof mapActions.setLayers;
  setBaseLayers: typeof mapActions.setBaseLayers;
  setBbox: typeof mapActions.setBbox;
  currentBbox: mapTypes.Bbox;
  mapId: string;
}

const Radar: React.FC<RadarProps> = ({
  mapId,
  setLayers,
  setBaseLayers,
  setBbox,
  currentBbox,
}: RadarProps) => {
  React.useEffect(() => {
    setLayers({ mapId, layers: [radarLayer] });
    setBaseLayers({ mapId, layers: [overLayer, baseLayerGrey] });
    setBbox({
      mapId,
      bbox: northernEuropeBbox.bbox,
      srs: northernEuropeBbox.srs,
    });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <MapViewConnect mapId={mapId} />
      <div
        style={{
          position: 'absolute',
          left: '10px',
          top: '65px',
          zIndex: 20,
        }}
      >
        <BboxComponent
          currentBbox={currentBbox}
          onClickButton1={(): void => {
            setBbox({
              mapId,
              bbox: northernEuropeBbox.bbox,
              srs: northernEuropeBbox.srs,
            });
          }}
          onClickButton2={(): void => {
            setBbox({
              mapId,
              bbox: toggleBbox.bbox,
              srs: toggleBbox.srs,
            });
          }}
        />
      </div>
    </>
  );
};

const ConnectedRadar = connect(
  (state, props) => ({ currentBbox: mapSelectors.getBbox(state, props.mapId) }),
  {
    setLayers: mapActions.setLayers,
    setBaseLayers: mapActions.setBaseLayers,
    setBbox: mapActions.setBbox,
  },
)(Radar);

const mapId = 'map-1';
const Body: React.FC = () => (
  <Provider store={store}>
    <ConnectedRadar mapId={mapId} />
  </Provider>
);

export default Body;
